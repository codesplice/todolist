//
//  EditItemViewController.swift
//  ToDoList
//
//  Created by Sam Ritchie on 16/05/2016.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit

class EditItemViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    var item: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let item = item {
            textField.text = item.text
            imageView.image = item.image ?? UIImage(named: "defaultPhoto")
            title = "Edit Item"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButtonTapped(sender: AnyObject) {
        if item == nil {
            item = Item(text: textField.text!, image: imageView.image)
        } else {
            item?.text = textField.text!
            item?.image = imageView.image
        }
        self.performSegueWithIdentifier("DismissSegue", sender: self)
    }
    
    @IBAction func imageTapped(sender: AnyObject) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .PhotoLibrary
        self.presentViewController(picker, animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imageView.image = image
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
}

