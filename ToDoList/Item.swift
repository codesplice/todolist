//
//  Item.swift
//  ToDoList
//
//  Created by Sam Ritchie on 16/05/2016.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit

class Item {
    var text: String
    var image: UIImage?
    
    init(text: String, image: UIImage?) {
        self.text = text
        self.image = image
    }
}