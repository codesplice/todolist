//
//  TableViewController.swift
//  ToDoList
//
//  Created by Sam Ritchie on 16/05/2016.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    var items = [Item]()

    override func viewDidLoad() {
        items = [
            Item(text: "One Item", image: nil),
            Item(text: "Another Item", image: nil)
        ]
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ItemCell", forIndexPath: indexPath) as! ItemCell
        let item = items[indexPath.row]
        cell.itemTextLabel.text = item.text
        cell.itemImageView.image = item.image
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let nav = segue.destinationViewController as? UINavigationController,
            let editView = nav.viewControllers.first as? EditItemViewController,
            let indexPath = tableView.indexPathForSelectedRow
            where segue.identifier == "EditItemSegue" {
                editView.item = items[indexPath.row]
        }
    }
    
    @IBAction func dismissItemEditView(segue: UIStoryboardSegue) {
        guard let editView = segue.sourceViewController as? EditItemViewController else { return }
        guard let item = editView.item else { return }
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Bottom)
        } else {
            items.append(item)
            tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: items.count - 1, inSection: 0)], withRowAnimation: .Bottom)
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            items.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Bottom)
        }
    }
}

class ItemCell: UITableViewCell {
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var itemTextLabel: UILabel!
}